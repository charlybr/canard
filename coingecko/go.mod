module ratonland.org/coingecko

go 1.15

require (
	github.com/go-chat-bot/bot v0.0.0-20201004141219-763f9eeac7d5
	github.com/olekukonko/tablewriter v0.0.5
	github.com/superoo7/go-gecko v1.0.0
)
