package coingecko

import (
	"errors"
	"fmt"
	"math"
	"strconv"
	"sync"
	"time"

	"github.com/go-chat-bot/bot"
)

type TrackedCoin struct {
	ID          string
	LastQueried time.Time
	LastUpdated time.Time
	LastPrice   float32
}

var tracked = make(map[string]TrackedCoin)
var trackedLock sync.Mutex
var threshold float32

const trackedExpire = time.Duration(4 * 24 * time.Hour)

func trackCoin(ID string) {
	trackedLock.Lock()
	if c, ok := tracked[ID]; !ok {
		tracked[ID] = TrackedCoin{
			ID:          ID,
			LastQueried: time.Now(),
		}
	} else {
		c.LastQueried = time.Now()
		tracked[ID] = c
	}
	trackedLock.Unlock()
}

func cmdTrackedList(_ *bot.Cmd) (msg string, err error) {
	trackedLock.Lock()
	for _, v := range tracked {
		msg += fmt.Sprintf("%s: queried on %s, updated on %s\n", v.ID,
			v.LastQueried.Format("2006-01-02 15:04"),
			v.LastUpdated.Format("2006-01-02 15:04"))
	}
	trackedLock.Unlock()

	if len(msg) == 0 {
		msg = "No tracked coins"
	}

	return
}

func cmdTrackRemove(cmd *bot.Cmd) (msg string, err error) {
	var notFound []string
	var removed []string

	if len(cmd.Args) < 1 {
		return "", errors.New("expecting at least 1 argument")
	}

	coins := expandListArgs(cmd.Args)
	trackedLock.Lock()
	for _, c := range coins {
		if _, ok := tracked[c]; !ok {
			notFound = append(notFound, c)
		} else {
			delete(tracked, c)
			removed = append(removed, c)
		}
	}
	trackedLock.Unlock()

	if len(removed) > 0 {
		msg = fmt.Sprintf("Removed: %v\n", removed)
	}
	if len(notFound) > 0 {
		msg += fmt.Sprintf("Not Found: %v\n", notFound)
	}

	return
}

func cmdTrackThreshold(cmd *bot.Cmd) (msg string, err error) {
	if len(cmd.Args) == 0 {
		return fmt.Sprintf("tracking threshold: %f%% / min", threshold), nil
	} else if len(cmd.Args) != 1 {
		return "", errors.New("expected 0 or 1 argument")
	}

	thr, err := strconv.ParseFloat(cmd.Args[0], 32)
	if err != nil {
		return "", errors.New("unable to parse threshold")
	}
	if thr <= 0 {
		return "", errors.New("invalid hreshold")
	}
	threshold = float32(thr)
	return fmt.Sprintf("Threshold for tracking alert set to %f%% / min", threshold), nil
}

func periodicCmdTrackedCheck() (ret []bot.CmdResult, err error) {
	var IDs []string

	trackedLock.Lock()
	for ID, tc := range tracked {
		if time.Since(tc.LastQueried) > trackedExpire {
			delete(tracked, ID)
			continue
		}
		IDs = append(IDs, ID)
	}
	trackedLock.Unlock()

	vc := []string{"usd"}
	sp, err := cg.SimplePrice(IDs, vc)
	if err != nil {
		fmt.Println("Unable to get prices for tracking:", err)
		return
	}

	trackedLock.Lock()
	for ID, p := range *sp {
		tc := tracked[ID]
		if tc.LastPrice != 0 {
			elapsedMins := time.Since(tc.LastUpdated).Minutes()
			percent := (((p["usd"] / tc.LastPrice) - 1.0) * 100.0)
			percMins := percent / float32(elapsedMins)
			if percMins > threshold || percMins < -threshold {
				msg := fmt.Sprintf("[!] %s moved %s%% in %d minutes (%f USD) [!]\n",
					ID, colorPercent(float64(percent)), int(math.Round(elapsedMins)), p["usd"])
				ret = append(ret, bot.CmdResult{
					Message: msg,
					Channel: defaultChannel,
				})
			}
		}
		tc.LastPrice = p["usd"]
		tc.LastUpdated = time.Now()
		tracked[ID] = tc

	}
	trackedLock.Unlock()

	return
}
